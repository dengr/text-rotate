(function () {
    var textBlock = document.getElementById('rtRotateBlock');
    var textElement = document.getElementById('rtRotateInput');

    document.getElementById('rtDoRotate').addEventListener('click', function () {
        // наличие элемента для вращения определяем по классу .rt-surface
        var hasRotateSurface = textBlock.classList.contains('rt-surface');
        // если элемент для вращения присутствует, то удаляем его
        // и возвращаем поле ввода текста
        if (hasRotateSurface) {
            document.getElementById('rtRotateSurface').remove();
            textBlock.classList.remove('rt-surface');
            textElement.classList.remove('rt-hide');
        } else {
            var textValue = textElement.value;
            if (!Boolean(textValue)) {
                return;
            }

            // скрываем поле ввода текста
            textElement.classList.add('rt-hide');
        
            // создаем элемент для вращения
            var rotateSurface = document.createElement('div');
            rotateSurface.setAttribute('id', 'rtRotateSurface');
            rotateSurface.innerHTML = textValue;
            textBlock.append(rotateSurface);
            // добавляем признак, что элемент для вращения создан и добавлен
            textBlock.classList.add('rt-surface');
            // применяем анимацию вращения
            rotateSurface.classList.add('rt-rotate');
        }
    });
})();
